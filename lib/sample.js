/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * Sample transaction processor function.
 * @param {org.acme.mynetwork.Trade} trade The trade instance.
 * @transaction
 */
function tradeCommodity(trade) {
//    var prevOwner = trade.commodity.owner;

   trade.commodity.owner = trade.newOwner;

    // Get the asset registry for the asset.
    return getAssetRegistry('org.acme.mynetwork.Commodity')
        .then(function (assetRegistry) {

            // Update the asset in the asset registry.
            return assetRegistry.update(trade.commodity);

        });
        // .then(function () {

        //     // Emit an event for the modified commodity.
        //     var event = getFactory().newEvent('org.acme.mynetwork', 'Traded');
        //     event.commodity = trade.commodity;
        //     event.oldOwner = prevOwner;
        //     event.newOwner = trade.newOwner;
        //     emit(event);

        // });

}
